<?php

namespace Drupal\config_safety\EventSubscriber;

use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Config\ConfigImportValidateEventSubscriberBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Update\UpdateHookRegistry;

class ConfigImportSubscriber extends ConfigImportValidateEventSubscriberBase {

  /**
   * The update hook registry.
   *
   * @var \Drupal\Core\Update\UpdateHookRegistry
   */
  protected $updateHookRegistry;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ConfigImportSubscriber constructor.
   *
   * @param \Drupal\Core\Update\UpdateHookRegistry $updateHookRegistry
   *   The update hook registry.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(UpdateHookRegistry $updateHookRegistry, ModuleHandlerInterface $moduleHandler) {
    $this->updateHookRegistry = $updateHookRegistry;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Checks that the configuration synchronization is valid.
   *
   * @param ConfigImporterEvent $event
   *   The config import event.
   */
  public function onConfigImporterValidate(ConfigImporterEvent $event) {
    $source = $event->getConfigImporter()->getStorageComparer()->getSourceStorage();

    if (!$source->exists('config_safety.schema_versions')) {
      return;
    }

    foreach ($source->read('config_safety.schema_versions') as $module => $update_version) {
      if (!$this->moduleHandler->moduleExists($module)) {
        continue;
      }

      $current_update_version = (int) $this->updateHookRegistry->getInstalledVersion($module);

      if ($current_update_version === UpdateHookRegistry::SCHEMA_UNINSTALLED) {
        continue;
      }

      if ($update_version < $current_update_version) {
        $event->getConfigImporter()->logError("The current Drupal installation contains a newer version of $module then what's in the config you're trying to import. Did you forget to export configuration after updating modules?");
      }

      if ($update_version > $current_update_version) {
        $event->getConfigImporter()->logError("The current Drupal installation contains a older version of $module then what's in the config you're trying to import.");
      }
    }
  }

}
